# Alle eingehenden Absender-Mailadressen als Empfänger vorschlagen

Aus den eingehenden Mailadressen wird ein MySQL-view erzeugt. Das kann dann als eine zusätzliche 
Datenquelle verwendet werden

## Dateien an bliebige Stelle hinterlegen

Dateien an beliebiger Stellle wo sie ausgeführt werden können

## Datenquelle in Kernel/Config.pm eintragen
otrs/Kernel/Config.pm ergänzen mit Inhalt von 
Config.pm-snippet


## eintragen in Crontab 
periodischen Aufruf in crontab eintragen z.B. in 

etc/crontab 

`1  *    * * *   root    sh /root/uniba.de/share/local/tasks/mysql/update-view/1.sh`


