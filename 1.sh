#!/bin/bash 
id_name="tasks-update-view"

pfad=`dirname $(readlink -f ${0})`
. "$pfad"/config.sh

mkdir -p $log_target
mkdir -p "$status_dir"
mkdir -p $temp_dir

#echo "$0 start" | mail -s "$id_name start "  $log_mail

if find $flagfile 
then        
        string=$id_name "flagfile present when starting \n" $flagfile
        echo "$string"
        echo "$string " | mail -s "stderr flagfile $id_name $0 "  $log_mail
        exit
else
        touch $flagfile
fi

mkdir -p "$com_dir"/block-system-restart/
echo $flagfile >> "$com_dir"/block-system-restart/"$id_name"


mysql --password=''  -v < "$pfad"/1.sql



#cat $log_target/1  | mail -s "stdout $string" $log_mail
#cat $log_target/2  | mail -s "stderr $string" $log_mail


rm "$com_dir"/block-system-restart/"$id_name"
rm $flagfile

#echo "$0 ende" | mail -s "$id_name ende $0 "  $log_mail