#SELECT COUNT(*) FROM otrs.ticket;

DROP VIEW IF EXISTS otrs.uniba_customer_from_mail;
CREATE VIEW otrs.uniba_customer_from_mail 
AS 
  SELECT 
  customer_id as login, 
  customer_id as email, 
  customer_id as customer_id, 
  "" as first_name,  "" as last_name,
  "1" as valid_id 
  FROM otrs.ticket 
  GROUP BY customer_id
;
SELECT * FROM otrs.uniba_customer_from_mail;